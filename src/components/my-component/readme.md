# my-component



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description     | Type     | Default     |
| -------- | --------- | --------------- | -------- | ----------- |
| `first`  | `first`   | The first name  | `string` | `'Test 2'`  |
| `last`   | `last`    | The last name   | `string` | `undefined` |
| `middle` | `middle`  | The middle name | `string` | `undefined` |


## CSS Custom Properties

| Name        | Description                                                         |
| ----------- | ------------------------------------------------------------------- |
| `--bg`      | Background color; Default value: var(--dlds--color--neutral--light) |
| `--padding` | Default value: var(--dlds--size--spacing--base)                     |
| `--text`    | Text color; Default value: var(--dlds--color--primary--dark)        |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
