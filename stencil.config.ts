import { Config } from '@stencil/core';
import { postcss } from '@stencil/postcss';
import { sass } from '@stencil/sass';
import autoprefixer from 'autoprefixer';

export const config: Config = {
  namespace: 'dlds',
  outputTargets: [
    {
      type: 'dist',
      esmLoaderPath: '../loader'
    },
    {
      type: 'docs-readme'
    },
    {
      type: 'www',
      serviceWorker: null // disable service workers
    }
  ],
  globalStyle: 'src/styles/theme/theme.scss',
  plugins: [
    postcss({
      plugins: [
        autoprefixer()
      ]
    }),
    sass({
      injectGlobalPaths: [
        'src/styles/config/shared.scss'
      ]
    })
  ],
};
